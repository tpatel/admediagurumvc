﻿function displayBarNotification(message, messagetype, timeout) {
    var barNotificationTimeout;
    if (message) {
        clearTimeout(barNotificationTimeout);
        //types: success, error, warning, info
        var cssclass = 'bar-success';
        if (messagetype == 'success') {
            cssclass = 'bar-success';
        } else if (messagetype == 'error') {
            cssclass = 'bar-error';
        } else if (messagetype == 'warning') {
            cssclass = 'bar-warning';
        } else if (messagetype == 'info') {
            cssclass = 'bar-info';
        }
        //remove previous CSS classes and notifications
        $('#bar-notification')
            .removeClass('bar-success')
            .removeClass('bar-error')
            .removeClass('bar-warning')
            .removeClass('bar-info');
        $('#bar-notification .content').remove();
        //add new notifications
        var htmlcode = '';
        if ((typeof message) == 'string') {
            htmlcode = '<p class="content">' + message + '</p>';
        } else {
            for (var i = 0; i < message.length; i++) {
                htmlcode = htmlcode + '<p class="content">' + message[i] + '</p>';
            }
        }
        $('#bar-notification').append(htmlcode)
            .addClass(cssclass)
            .fadeIn('slow')
            .mouseenter(function () {
                clearTimeout(barNotificationTimeout);
            });

        $('#bar-notification .close').unbind('click').click(function () {
            $('#bar-notification').fadeOut('slow');
        });
        //timeout (if set)
        if (timeout > 0) {
            barNotificationTimeout = setTimeout(function () {
                $('#bar-notification').fadeOut('slow');
            }, timeout);
        }
    }
}