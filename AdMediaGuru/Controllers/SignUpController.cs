﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdMediaGuru.Controllers
{
    public class SignUpController : Controller
    {
        #region Variable
        admedia_guruEntities adguru = new admedia_guruEntities();
        #endregion

        /// <summary>
        /// SignUp
        /// </summary>
        /// <returns></returns>
        public ActionResult SignUp()
        {
            var getState = adguru.SubCategories.Where(c => c.category_id == 1).ToList();
            ViewBag.stateInfo = getState;
            Models.User user = new Models.User();
            return View(user);
        }

        [HttpPost]
        public ActionResult SignUp(Models.User user)
        {
            try
            {
                bool checkUser = CheckIfUserRegistar(user.user_emailId);
                if (checkUser == false)
                {
                    User userdetails = new User();
                    if (user.user_id > 0)
                        userdetails.user_state = user.user_state;
                    userdetails.user_company_name = user.user_company_name;
                    userdetails.user_name = user.user_name;
                    userdetails.user_emailId = user.user_emailId;
                    userdetails.user_phone = user.user_phone;
                    userdetails.user_password = user.user_password;
                    userdetails.is_deleted = false;
                    userdetails.created_date = DateTime.UtcNow;
                    adguru.Users.Add(userdetails);
                    adguru.SaveChanges();
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("UnSuccess", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
        }

        public bool CheckIfUserRegistar(string emailId)
        {
            return (adguru.Users.Count(user => user.user_emailId == emailId && user.is_deleted == false) > 0 ? true : false);
        }
    }
}