﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdMediaGuru.Controllers
{
    public class HomeController : Controller
    {
        #region Variable
        admedia_guruEntities adguru = new admedia_guruEntities();
        #endregion
        public ActionResult Home()
        {
            return View();
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="emailId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public ActionResult Login(string emailId,string password)
        {
            try
            {
                int userCount = adguru.Users.Where(c => c.user_emailId == emailId && c.user_password == password).Count();
                if(userCount>0)
                    return Json("Success", JsonRequestBehavior.AllowGet);
                else
                    return Json("Failure", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
            }
            return Json("Failure", JsonRequestBehavior.AllowGet);
        }
    }
}