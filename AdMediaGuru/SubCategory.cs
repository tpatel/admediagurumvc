//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdMediaGuru
{
    using System;
    using System.Collections.Generic;
    
    public partial class SubCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SubCategory()
        {
            this.SubCategory1 = new HashSet<SubCategory>();
        }
    
        public int sub_category_id { get; set; }
        public Nullable<int> category_id { get; set; }
        public string category_text { get; set; }
        public string category_description { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<int> is_parentId { get; set; }
    
        public virtual Category Category { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubCategory> SubCategory1 { get; set; }
        public virtual SubCategory SubCategory2 { get; set; }
    }
}
